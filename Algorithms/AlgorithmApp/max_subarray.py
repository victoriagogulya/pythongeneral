from __future__ import division
import pytest


def find_max_subarray(array, idx_low, idx_high):
    """Search for a subarray with maximum sum of elements.
       T(n) = O(nlog(n))
    """

    if idx_high == idx_low:
        return idx_low, idx_high, array[idx_low]

    idx_mid = (idx_low + idx_high) // 2
    left_low, left_high, left_sum = find_max_subarray(array, idx_low, idx_mid)
    right_low, right_high, right_sum = find_max_subarray(array, idx_mid + 1, idx_high)
    cross_low, cross_high, cross_sum = _find_max_crossing_subarray(array, idx_low, idx_mid, idx_high)

    if left_sum >= right_sum and left_sum >= cross_sum:
        return left_low, left_high, left_sum
    if right_sum >= left_sum and right_sum >= cross_sum:
        return right_low, right_high, right_sum
    return cross_low, cross_high, cross_sum


def _find_max_crossing_subarray(array, idx_low, idx_mid, idx_high):
    left_sum = array[idx_mid]
    max_left = idx_mid
    sum_current = left_sum
    for i in range(idx_mid - 1, idx_low - 1, -1):
        sum_current += array[i]
        if sum_current > left_sum:
            left_sum = sum_current
            max_left = i

    idx_mid += 1
    right_sum = array[idx_mid]
    max_right = idx_mid
    sum_current = right_sum
    for i in range(idx_mid + 1, idx_high + 1):
        sum_current += array[i]
        if sum_current > right_sum:
            right_sum = sum_current
            max_right = i

    return max_left, max_right, left_sum + right_sum


@pytest.mark.parametrize("array, idx_low, idx_high, expected_sum",
                         [([0, 1, 0, -1, 3, 0, 2, 1, -1, 0], 4, 7, 6),
                          ([2, 0, 4, -1, 4, 0, -2, 0, 1, 0], 0, 4, 9),
                          ([2, 0, 4, -1, 0, 0, -2, 0, -1, 0], 0, 2, 6),
                          ([2, 0, -4, -1, 0, 0, 2, 4, 0, -1], 6, 7, 6)])
def test_find_max_subarray(array, idx_low, idx_high, expected_sum):
    assert (idx_low, idx_high, expected_sum) == find_max_subarray(array, 0, len(array) - 1)
